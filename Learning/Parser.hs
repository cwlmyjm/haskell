module Parser where

import ST

type Token = Char

type Parser a = ST String a 

type Derivation = Token -> [Token]

data Pattern = Pattern  [Token] 
                        [Token]
                        [Derivation]
                        Token

type Machine a = Pattern -> Parser a

runMachine :: Machine a -> String -> a
runMachine = undefined

parserS :: [Token] -> Maybe [Token]
parserS ts = case parserA ts of
                 Nothing -> Nothing
                 Just ts' -> case parserB ts' of
                                 Nothing -> Nothing
                                 Just ts'' -> Just ts''

parserA :: [Token] -> Maybe [Token]
parserA ('a':ts) = Just ts
parserA _ = Nothing

parserB :: [Token] -> Maybe [Token]
parserB ('b':ts) = Just ts
parserB _ = Nothing

