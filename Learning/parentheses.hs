module Main where

p1 = "([{"
p2 = "}])"

main = do
	x <- getLine
	print $ f [] x

f [] [] = True
f [] (a:as) =  if elem a p1 then f [a] as else False
f (a:as) (b:bs)	| elem b p1 = f (b:a:as) bs
				| check_pair a b = f as bs
				| otherwise = False 
f _ [] = False
				
check_pair '(' b = b == ')'
check_pair '[' b = b == ']'
check_pair '{' b = b == '}'