module Tree where

data Tree a = Node a (Tree a) (Tree a) | Empty deriving Show

type BTree a = Tree a

add :: BTree Int -> Int -> BTree Int
add Empty a = Node a Empty Empty
add (Node n left right) a = if a > n then Node n left (add right a) else Node n (add left a) right

remove :: BTree Int -> Int -> BTree Int
remove = undefined
remove Empty _ = Empty
remove (Node n left right) a = undefined
