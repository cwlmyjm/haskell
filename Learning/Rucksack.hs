module Rucksack where

rucksack a b = filter (\x -> sum x == b) (ff $ subSeq a)

ff :: [[a]] -> [[a]]
ff a = concat [filter (\y -> length y == x) a|x <- [1..wow]] 
                where wow = maximum (map length a)

subSeq [a] = [[a]]
subSeq (a:as) = x ++ y ++ [p ++ q|p <- x ,q <- y]
                where 
                    x = subSeq [a] 
                    y = subSeq as

subSeq' [a] = [[a]]
subSeq' (a:as) = ([a]:) $ (merge x) $ map (a:) x where x = subSeq' as                    
                    
rucksack' [] 0      = [[]]
rucksack' [] n      = []
rucksack' (x:xs) n  = merge (rucksack' xs n) (map (x:) (rucksack' xs (n - x)))

merge :: [[a]] -> [[a]] -> [[a]]
merge a [] = a
merge [] b = b
merge (a:as) (b:bs) | length a > length b = b : merge (a:as) bs
                    | otherwise = a : merge as (b:bs)
