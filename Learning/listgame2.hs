module Main where


main = do
	x <- getLine
	print $ f (read x :: Integer) 2

f :: Integer -> Integer -> Integer
f m n = if m < n then 0 
		else 1 + (f (div m a) (a+1)) where a = g m n

-- f :: Int -> Int -> Int	
-- f 1 _ = 0
-- f 2 _ = 1
-- f m n = 1 + (f (div m a) a)
		-- where
			-- a = g m n

g m n	| (mod m n) == 0 = n
		| (n*n) > m = m
		| otherwise = g m (n+1)