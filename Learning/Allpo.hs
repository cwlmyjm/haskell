module Allpo where

allpo :: [a] -> [[a]]
allpo = undefined

data Tree a = Node a [Tree a] deriving Show

toLists :: Tree a -> [[a]]
toLists (Node a []) = [[a]]
toLists (Node a as) = map (a:) (concat $ map toLists as)

a :: Tree Int
a = Node 1 [Node 2 [Node 5 [],Node 6 []],Node 3 [Node 7 [],Node 8 []], Node 4 [Node 9 [],Node 10 []]]
