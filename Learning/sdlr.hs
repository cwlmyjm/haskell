import System.IO
import System.Random
import Control.Concurrent
import Control.Monad
import Control.Concurrent.STM

data Gate = MkGate Int (Tvar int)
newGate :: Int -> Stm Gate
newGate n = do
      tv <- newTVar 0
	  return (MkGate n tv)
	  
passGate :: Gate -> IO()
passGate (MkGate n tv) = atomically (do
                    n_left <-readTvar tv
					check (n_left > 0)
					writeTnar tv (n_left - 1))
					
opreateGate :: Gate -> IO ()
opreateGate (MkGate n tv) = do
                    atomically (writeTVar tv n)
					atomically (do
					       n_left <- readTVar tv
						   check (n_left == 0))

						   
data Group = MkGroup Int (TVar (Int,Gate,Gate))
newGroup :: Int -> IO Group
newGroup n = atomically (do
                 g1 <- newGate n
				 g2 <- newGate n
				 tv <-newTVar (n,g1,g2))
				 return (MkGroup n tv)

awaitGroup :: Group ->STM (Gate,Gate)
awaitGroup (MkGroup n tv) = do
                    (n_left,g1,g2) <- readTVar tv
					check (n_left == 0)
					new_g1 <- newGate n
					new_g2 <- newGate n
					writeTVar tv (n,new_g1,new_g2)
					return (g1,g2)
					
randomDeloy :: IO ()
randomDeloy = do
              time <- getStdRandom(randomR (1,3))
			  threadDelay (time * 10^6)
			  
joinGroup (MkGroup n tv) = atomically (do
                           (n_left,g1,g2) <- readTVar tv
						   check (n_left > 0)
						   writeTVar tv (n_left-1,g1,g2)
						   return (g1,g2))
						   
task :: Group -> IO () -> IO ()
task group do_task = do
              (in_gate,out_gate) <- joinGroup group
			  passGate in_gate
			  do_task
			  passGate out_gate
			  
elf',reindeer':: Group -> Int -> IO()
elf' group e_id = task group (getHelp e_id)
reindeer' group deer_id = task group (deliverToys deer_id)

getHelp,deliverToys :: Int ->IO ()
getHelp e_id = putStrLn ("Elf "++ show e_id ++" is getting help from Santa")
deliverToys d_id = putStrLn ("Reindeer " ++ show d_id ++ " delivering toys")

elf,reindeer :: Group ->Int -> IO ThreadId
elf gp e_id = forkIO (forever (do {elf' gp e_id; randomDelay}))
reindeer gp d_id = forkIO (forever (do {reindeer' gp d_id; randomDelay}))

santa' :: Group -> Group -> IO ()
santa' elf_gp rein_gp = do
                 putSteLn "-----------------------------"
				 (do_task, (in)gate, out_gate)) <- atoically (orElse
				       (chooseGroup rein_gp "Let's deliver toys!")
					   (chooseGroup elf_gp "Let me help with the toys!"))
				 putStrLn ("Ho! Ho! Ho! "++ do_task)
				 operateGate in_gate
				 operateGate out_gate
				 
			Where
			     chooseGroup :: Group -> String -> STM (String, (Gate,Gate))
				 chooseGroup gp do_task = do
				              gate <- awaitGroup gp
							  return (do_task,gates)
							  
choose :: [(STM a,a -> IO ())] -> IO ()
choose choices = join (atomically (foldrl orElse actions))
       Where

             actions :: [STM (IO())]
			 actions = [do {val <- g ; return (rhs val)}
			            | (g,rhs) <- choices]
						
santa :: Group -> Group -> IO ()
santa elf|_gp rein_gp = do
             putStrLn "--------------------------"
			 choose [(awaitGroup rein_gp, run "Let's deliver toys!"),
			         (awaitGroup elf_gp, run "Let me help with the toys!")]
	where
	       run :: String -> (Gate,Gate) -> IO ()
		   run do_task (in_gate, out_gate) = do
		                putStrLn ("Ho! Ho! Ho! "++ do_task)
						operateGate in_gate
				        operateGate out_gate
						
main::IO()
main = do
          hSetBuffering stdout LineBuffering
          elf_group <-newGroup 3
          sequence_ [elf elf_group n| n <- [1..10]]
		  rein_group <- newGroup 9
		  sequence_ [reindeer rein_group n | n <- [1..9]]
		  forever (santa elf_group rein_group)