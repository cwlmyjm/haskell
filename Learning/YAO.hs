module Yao where

type Name   = Char

data Op     =     Add 
                | Sub 
                | Mul 
                | Div

data Expr   =     Val Int 
                | Var Name 
                | App Op Expr Expr 
                | Equ Expr Expr

data Prog   =     If Expr Prog Prog 
                | While Expr Prog 
                | Assign Name Expr 
                | Seqn [Prog] 
                | For Expr Prog Prog
                | Switch Expr [(Expr,Prog)]

type Stack  =   [Int]

type Mem    =   [(Name,Int)]

type Code   =   [Inst] 

data Inst   =     PUSH Int 
                | PUSHV Name 
                | POP Name 
                | DO Op 
                | JUMP Label 
                | JUMPZ Label 
                | LABEL Label 

type Label = Int
