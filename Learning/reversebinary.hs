module Main where

f :: Int -> [Int] -> [Int]
f 0 is = is
f n is = f (div n 2) ((mod n 2):is)


g :: [Int] -> [Int] -> Int
g [] bs = 0
g (a:as) (b:bs) = a * b + g as bs

h :: [Int]
h = iterate (*2) 1

main :: IO()
main = do
		x <- getLine
		print $ g (f (read x :: Int) []) h