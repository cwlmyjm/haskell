module Machine where

import ST

newtype Machine input output = Machine(input -> output)

type Stage = Maybe Char

type Input = Maybe Char

type Transition = Stage -> Input -> Stage

type Transition2 = (Stage,Input,Stage)

data DFA = DFA [Stage] [Input] [Transition] Stage [Stage]

toTransition :: Char -> Char -> Char -> Transition
toTransition s1 inp s2 = (\s -> 
                          \i -> 
                            case s of
                                Just s -> if s == s1 then 
                                            case i of
                                                Just i ->   if i == inp then Just s2
                                                            else Nothing
                                                Nothing -> Nothing
                                          else Nothing                   
                                Nothing -> Nothing)
