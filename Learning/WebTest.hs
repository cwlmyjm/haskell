module Main where 

import Network.CGI
-- import Database.MySQL

cgiMain :: CGI CGIResult
cgiMain = do 
            x <- serverName
            output x
--cgiMain = redirect "http://www.baidu.com/"
--cgiMain = output "Hello World!"

main :: IO ()
main = runCGI (handleErrors cgiMain)
