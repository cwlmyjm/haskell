module WriterTest where

import ST
import Control.Monad.Writer

half :: Int -> Writer String Int
half x = do 
        tell ("I just halved " ++ (show x) ++ "!")
        return (div x 2)

div' :: Int -> Int -> Int
div' = div

--divBy3 :: Int -> Writer Int Int
divBy3 x = do
        tell(1)
        return (div' x 3)
        
class Wid a where
        wempty  :: a

        wappend :: a -> a -> a

        wconcat :: [a] -> a

        wconcat = foldr wappend wempty
     

half' :: Int -> WR String Int
half' x = WR(div' x 2,"...")

newtype Array a = Array [a] deriving Show

emptyArray :: Array a
emptyArray = Array []

connectArray :: Array [a] -> Array a
connectArray (Array a) = Array $ concat a

appendArray :: Array a -> Array a -> Array a
appendArray (Array a) (Array b) = Array (a ++ b)

newtype WR w a = WR(a,w) deriving Show

runWR :: WR w a -> a
runWR (WR(a,w)) = a

appWR :: WR w a -> (a,w)
appWR (WR(a,w)) = (a,w)


instance (Monoid w) => Functor (WR w) where
  -- fmap                           :: (a -> b) -> WR w a -> WR w b
  fmap f (WR(a,w))                  = WR(f a,w)
  
divWR :: Int -> Int -> WR String Int
divWR a b = WR(div a b,show a ++ " div " ++ show b)

f :: WR Int Int
f = WR(100,100)

instance (Monoid w) => Applicative (WR w) where
  -- pure                           :: a -> WR s a
  pure a                            = WR(a,mempty)
  -- <*>                            :: WR w (a -> b) -> WR w a -> WR w b
  wrf <*> wrx                       = WR(let (a,w)   = appWR wrf
                                             (a',w') = appWR wrx in (a a',mappend w w'))

instance (Monoid w) => Monad (WR w) where
    -- (>>=)                          :: WR w a -> (a -> WR w b) -> WR w b
    wr >>= f                          = WR(let (a,w) = appWR wr 
                                               (b,w') = appWR $ f a in (b,mappend w w'))

idInWR :: Int -> WR [Int] Int
idInWR b = WR(b,[b])