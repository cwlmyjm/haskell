module CSC322a2TestData where

tests :: [(Integer, [Int], Bool)]
tests = [(1, [5, 3, 1, 6, 8, 2, 4, 7], True),
         (2, [3, 6, 2, 5, 8, 1, 7, 4], True),
         (3, [2, 5, 11, 4, 1, 1, 10, 3], False),
         (4, [6, 6, 1, 4, 7, 5], False),
         (5, [5, 3, 1, 7, 2, 8, 6, 4], True),
         (6, [1, 2, 2, 1, 6], False),
         (7, [3, 6, 2, 5, 8, 1, 7, 4], True),
         (8, [7, 5, 3, 1, 6, 8, 2, 4], True),
         (9, [1, 5, 2, 3, 3], False),
         (10, [3, 5, 5, 11, 9, 4, 8, 4], False),
         (11, [6, 4, 1, 5, 8, 2, 7, 3], True),
         (12, [8, 6, 5, 3, 1, 4], False),
         (13, [5, 6, 2, 2, 1], False),
         (14, [6, 1, 8, 7, 3, 8, 1], False),
         (15, [3, 6, 7, 2, 1, 4, 7], False),
         (16, [6, 4, 7, 1, 3, 5, 2, 8], True),
         (17, [3, 8, 9, 8, 4, 4, 9], False),
         (18, [2, 4, 6, 1, 3, 5], True),
         (19, [5, 8, 4, 1, 7, 2, 6, 3], True),
         (20, [6, 1, 8, 4, 9, 9, 6], False),
         (21, [2, 5, 7, 2, 2, 7], False),
         (22, [3, 5, 8, 4, 1, 7, 2, 6], True),
         (23, [4, 3, 9, 4, 6, 3, 2], False),
         (24, [3, 2, 6, 6, 7, 6, 3], False),
         (25, [7, 3, 6, 2, 5, 1, 4], True),
         (26, [2, 4, 6, 1, 3, 5, 7], True),
         (27, [5, 4, 9, 11, 1, 3, 11, 3], False),
         (28, [2, 6, 3, 7, 4, 1, 5], True),
         (29, [5, 2, 4, 7, 3, 8, 6, 1], True),
         (30, [4, 5, 3, 4, 2], False),
         (31, [4, 2, 7, 5, 1, 8, 6, 3], True),
         (32, [1, 7, 4, 6, 8, 2, 5, 3], True),
         (33, [6, 3, 1, 4, 7, 5, 2], True),
         (34, [6, 11, 2, 7, 6, 4, 4, 10], False),
         (35, [1, 7, 5, 7, 3, 7], False),
         (36, [2, 6, 6, 2, 6], False),
         (37, [6, 1, 6, 6, 2], False),
         (38, [3, 6, 8, 1, 5, 7, 2, 4], True),
         (39, [6, 2, 5, 1, 4, 7, 3], True),
         (40, [6, 1, 1, 3, 5, 2], False),
         (41, [7, 8, 6, 8, 4, 7, 1], False),
         (42, [7, 4, 2, 8, 6, 1, 3, 5], True),
         (43, [2, 1, 7, 2, 6, 1], False),
         (44, [3, 1, 7, 5, 8, 2, 4, 6], True),
         (45, [1, 5, 6, 1, 8, 5], False),
         (46, [2, 3, 2, 1, 4, 4, 8], False),
         (47, [6, 4, 1, 3, 2], False),
         (48, [9, 4, 1, 8, 8, 8, 7], False),
         (49, [4, 6, 1, 5, 2, 8, 3, 7], True),
         (50, [3, 11, 9, 5, 9, 10, 9, 2], False),
         (51, [4, 2, 5, 3, 1], True),
         (52, [1, 1, 1, 2, 6], False),
         (53, [7, 2, 4, 6, 1, 3, 5], True),
         (54, [9, 9, 7, 1, 5, 7, 8, 2], False),
         (55, [8, 2, 4, 1, 7, 5, 3, 6], True),
         (56, [5, 3, 1, 7, 2, 8, 6, 4], True),
         (57, [4, 5, 3, 5, 3], False),
         (58, [8, 6, 4, 4, 7, 3], False),
         (59, [6, 4, 4, 5, 4, 1], False),
         (60, [5, 5, 4, 11, 8, 8, 3, 9], False),
         (61, [2, 4, 1, 4, 4], False),
         (62, [3, 5, 7, 2, 4, 6, 4], False),
         (63, [7, 6, 2, 7, 8, 9, 2, 4], False),
         (64, [6, 8, 2, 4, 1, 7, 5, 3], True),
         (65, [7, 4, 2, 8, 6, 1, 3, 5], True),
         (66, [9, 7, 3, 8, 1, 1, 8], False),
         (67, [3, 5, 2, 2, 1], False),
         (68, [6, 4, 2, 8, 5, 7, 1, 3], True),
         (69, [7, 3, 1, 6, 8, 5, 2, 4], True),
         (70, [4, 7, 5, 3, 1, 6, 8, 2], True),
         (71, [5, 7, 1, 4, 2, 8, 6, 3], True),
         (72, [5, 9, 2, 3, 9, 5, 3, 3], False),
         (73, [2, 4, 6, 1, 3, 5], True),
         (74, [7, 3, 1, 6, 8, 5, 2, 4], True),
         (75, [3, 6, 8, 1, 5, 7, 2, 4], True),
         (76, [2, 5, 3, 3, 4], False),
         (77, [1, 4, 2, 5, 3], True),
         (78, [4, 6, 5, 5, 2], False),
         (79, [2, 1, 3, 1, 3, 1, 1], False),
         (80, [7, 5, 3, 1, 6, 4, 2], True),
         (81, [1, 4, 8, 3, 5, 1], False),
         (82, [3, 6, 8, 1, 4, 7, 5, 2], True),
         (83, [7, 4, 2, 5, 8, 1, 3, 6], True),
         (84, [3, 5, 7, 2, 4, 6, 1], True),
         (85, [6, 4, 2, 8, 5, 7, 1, 3], True),
         (86, [4, 2, 7, 5, 3, 1, 6], True),
         (87, [4, 6, 8, 2, 7, 1, 3, 5], True),
         (88, [2, 7, 11, 2, 1, 3, 6, 9], False),
         (89, [7, 6, 1, 8, 9, 9, 4], False),
         (90, [8, 8, 7, 1, 2, 2, 6], False),
         (91, [6, 8, 2, 4, 1, 7, 5, 3], True),
         (92, [3, 5, 2, 4, 1], True),
         (93, [6, 4, 7, 1, 3, 5, 2], True),
         (94, [2, 7, 5, 8, 1, 4, 6, 3], True),
         (95, [7, 10, 2, 5, 11, 7, 5, 2], False),
         (96, [3, 6, 6, 4, 3], False),
         (97, [5, 6, 8, 7, 7, 8, 4], False),
         (98, [7, 3, 6, 2, 5, 1, 4], True),
         (99, [1, 4, 7, 3, 6, 2, 5], True),
         (100, [3, 1, 4, 2], True)]
