module CSC322a2 where

queens :: [Int] -> Bool
queens (xs) = different xs && not (canAttackAny (zip xs [1..]))
-- queens (xs) = not (canAttackAny (zip xs [1..]))

different :: [Int] -> Bool
different [] = True
different (a:as) = if elem a as then False else different as

canAttackAny :: [(Int,Int)] -> Bool
canAttackAny [] = False
canAttackAny (a:as) = if or $ map (canAttackOne a) as then True else canAttackAny as

canAttackOne :: (Int,Int) -> (Int,Int) -> Bool
canAttackOne (a1,a2) (b1,b2) = (a1 - a2 == b1 - b2) || (a1 + a2 == b1 + b2)
-- canAttackOne (a1,a2) (b1,b2) = (a1 == b1) || (a2 == b2) || (a1 - a2 == b1 - b2) || (a1 + a2 == b1 + b2)
