module CSC322a2tester where

import CSC322a2
import CSC322a2TestData

testOne q r = queens q == r
testAll tests = [(i, testOne q r) | (i, q, r) <- tests]
result = testAll tests
score = sum [1 | (i, x) <- result, x == True] * 0.2
passed = [i | (i, x) <- result, x == True]
failed = [i | (i, x) <- result, x == False]
results = (score, passed, failed)
