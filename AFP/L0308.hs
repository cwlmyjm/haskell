module L0308 where

import L0307 hiding (fresh,rlabel)

rlabel :: Tree a -> Int -> (Tree Int,Int)
rlabel (Leaf _) n = (Leaf n,n+1)
rlabel (Node l r) n = let (l1,n') = rlabel l n
                          (r1,n'') = rlabel r n' in (Node l1 r1,n'')

-- rlabel' :: Tree a -> Int -> Tree Int

fresh :: ST Int Int
fresh = ST (\n -> (n,n+1))
-- fresh = do 
--    n <- get id
--    update (+1)
--    return n

alabel :: Tree a -> ST Int (Tree Int)
-- alabel (Leaf _) = ST (\n -> (Leaf n,n+1))
alabel (Leaf _) = Leaf <$> fresh
-- alabel (Leaf _) = pure Leaf <*> fresh
alabel (Node l r) = Node <$> alabel l <*> alabel r

mlabel :: Tree a -> ST Int (Tree Int)
mlabel (Leaf _) = do
                     n <- fresh
                     return (Leaf n)
mlabel (Node l r) = do
                      l' <- mlabel l
                      r' <- mlabel r
                      return (Node l' r')

fib :: Integer -> Integer
fib 0 = 0
fib 1 = 1
fib n = fib (n-1) + fib (n-2)

fiblist = 0:1:[fiblist!!x + fiblist!!(x+1)|x <- [0..]]

nextfib :: ST (Integer,Integer) Integer
nextfib = ST(\(x,y) -> let z = x + y in (z,(y,z)))

newfibpair :: Integer -> (Integer,Integer)
newfibpair 0 = (0,0)
newfibpair 1 = (0,1)
newfibpair n = snd $ app nextfib (newfibpair (n-1))

newfib n = snd $ newfibpair n
