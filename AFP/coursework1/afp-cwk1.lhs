G52AFP Coursework 1 - Noughts and Crosses
   
Your full name(s)
Your full email address(es)

----------------------------------------------------------------------

The 3x3 board is represented as a list of list of player values:

> type Board                    =  [[Player]]

In turn, a player value is either a nought, a blank, or a cross, with
a blank representing a space on the board that is not yet occupied:

> data Player                   =  Nought | Blank | Cross
>                                  deriving (Ord, Eq, Show)

The following code displays a board on the screen:

> showBoard                     :: Board -> IO ()
> showBoard                     =  putStrLn . unlines . concat . interleave bar . map showRow
>                                  where
>                                     bar = [replicate 18 '-']
>
> showRow                       :: [Player] -> [String]
> showRow                       =  beside . interleave bar . map showPlayer
>                                  where
>                                     beside = foldr1 (zipWith (++))
>                                     bar    = replicate 5 "|"
>
> showPlayer                    :: Player -> [String]
> showPlayer Nought             =  ["     ", " +-+ ", " | | ", " +-+ ", "     "]
> showPlayer Blank              =  ["     ", "     ", "     ", "     ", "     "]
> showPlayer Cross              =  ["     ", " \\ / ", "  X  ", " / \\ ", "     "]
>
> interleave                    :: a -> [a] -> [a]
> interleave x []               =  []
> interleave x [y]              =  [y]
> interleave x (y:ys)           =  y : x : interleave x ys

----------------------------------------------------------------------
