module MyLibrary where

-- input a number list
-- if there exists one number big than 0 then return the first one in a list
-- else return the input list
find :: [Int] -> [Int]
find a = if null b then a else b where b = filter (>0) a


wow = getLine

f1 = do
	x<-getLine
	return 1
		
getInt = do
	x <- getLine
	if (null x) then
		getInt
	else if (not $ check x) then
		getInt
	else
		return x
	
check :: String -> Bool
check a = and $ map (flip elem "0123456789") a


f2 :: [(Int,Int)] -> Int -> [(Int,Int)]
f2 [] _ = []
f2 (a:as) b = if (m == b) then a:f2 as b else f2 as b where (m,n) = a

f3 :: [(Int,Int)] -> (Int,Int)
f3 [a] = a
f3 (a:as) = if m > m1 then a else f3 as
			where 
				(m,n) = a
				(m1,n1) = f3 as
				
f4 = getChar

f5 :: Char -> IO Bool
f5 a = return True