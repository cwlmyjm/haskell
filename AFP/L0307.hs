module L0307 where

newtype ST s a = ST (s -> (a,s))

a = ST (\x -> (even x,x))
b = ST (\x -> (not,x))
c = ST (\x -> (id,x))
d = ST (\x -> (map,x))
e = ST (\x -> ([x],x))

st' :: c -> ST s a
st' = undefined

app :: ST s a -> s -> (a,s)
app (ST f) = f

run :: ST s a -> s -> a
--run st s = fst (app st s)
run st = fst . app st

update :: (s -> s) -> ST s ()
update f = ST (\s -> ((),f s))

get :: (s -> a) -> ST s a
get f = ST (\s -> (f s,s))

instance Functor (ST s) where
  -- fmap :: (a -> b) -> ST s a -> ST s b
  fmap f st = ST(\s ->let (a,s') = app st s in (f a,s'))

instance Applicative (ST s) where
  -- pure :: a -> ST s a
  pure a = ST (\s -> (a,s))
  
  -- <*> :: ST s (a -> b) -> ST s a -> ST s b
  stf <*> stx = ST(\s -> 
            let (f,s') = app stf s
                (x,s'') = app stx s' in (f x,s''))

instance Monad (ST s) where
  -- >>= :: ST s a -> (a -> ST s b) -> ST s b
  st >>= f = ST (\s -> let (x,s') = app st s in app (f x) s')
  
data Tree a = Leaf a | Node (Tree a) (Tree a) deriving Show

tree :: Tree Char
tree = Node (Node (Leaf 'a') (Leaf 'b')) (Leaf 'c')

rlabel :: Tree a -> Int -> (Tree Int,Int)
rlabel (Leaf _) n = (Leaf n,n+1)
rlabel (Node l r) n = (Node l' r',n'')
                     where
                     (l',n') = rlabel l n
                     (r',n'') = rlabel r n'

fresh :: ST Int Int
fresh = undefined
