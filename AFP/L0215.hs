module L0215 where


data Tree1 a = Leaf1 a | Node1 (Tree1 a) (Tree1 a) deriving Show
data Tree2 a = Empty | Node2 (Tree2 a) a (Tree2 a) deriving Show
data Tree3 a = Node3 a [Tree3 a] deriving Show

a = Node1 (Leaf1 1) (Node1 (Leaf1 2) (Leaf1 3))
b = Node2 (Node2 Empty 2 Empty) 1 (Node2 Empty 3 Empty)
c = Node3 1 [Node3 2 [],Node3 3 [],Node3 4 []]


instance Functor Tree1 where
 fmap f (Leaf1 a) = Leaf1 (f a)
 fmap f (Node1 a b) = Node1 (fmap f a) (fmap f b)

 
instance Functor Tree2 where
 fmap f Empty = Empty
 fmap f (Node2 a b c) = Node2 (fmap f a) (f b) (fmap f c)


instance Functor Tree3 where
 fmap f (Node3 a b) = Node3 (f a) (map (fmap f) b)


data BQ = QWQ | QAQ | QOQ | QVQ 

instance Eq BQ where
 (==) QWQ QWQ = True
 (==) QAQ QAQ = True
 (==) QOQ QOQ = True
 (==) QVQ QVQ = True
 (==) _ _ = False

instance Show BQ where
 show QWQ = "QWQ"
 show QAQ = "QAQ"
 show QOQ = "QOQ"
 show QVQ = "QVQ"