module Math where


data Operation = Plus | Sub | Mult | Power

data Expr = Number Int | Symbol Operation | Bracket Equation

instance Show Expr where
  show (Number e) = show e
  show (Symbol Plus) = "+"
  show (Symbol Sub) = "-"
  show (Symbol Mult) = "*"
  show (Symbol Power) = "^"
  show (Bracket e) = "(" ++ (concat (map show e)) ++")"

type Equation = [Expr]

printEq :: Equation -> IO()
printEq e = putStrLn $ concat $ map show e

eval :: Equation -> Int
eval [Number e] = e
eval (Number e:Symbol Plus:es) = e + eval es
eval (Number e:Symbol Sub:es) = e - eval es
eval (Number e1:Symbol Mult:Number e2:es) = eval (Number (e1 * e2) : es)
eval (Number e1:Symbol Mult:Bracket e2:es) = eval (Number (e1 * (eval e2)) : es)
eval (Bracket e:es) = eval $ Number (eval e) : es
eval (Number e1:Symbol Power:Number e2:es) = eval (Number (power e1 e2) : es)
eval (Number e1:Symbol Power:Bracket e2:es) = eval (Number (power e1 (eval e2)) : es)

power :: Int -> Int -> Int
power _ 0 = 1
power a b = a * power a (b-1)

b = [Number 3,Symbol Power,Number 5]
a = [Number 2,Symbol Power,Bracket [Number 3,Symbol Power,Number 5]]
