-- Examples of functors (and not) and applicatives
-- Venanzio Capretta, February 2016

-- Pairs of elements of type a

newtype Pair a = Pair (a,a)
  deriving (Eq,Ord,Show,Read)

instance Functor Pair where
  fmap f (Pair (x,y)) = Pair (f x, f y)

-- wrong
instance Applicative Pair where
  pure a = Pair (a,a)

  (Pair (a1,a2)) <*> (Pair (a3,a4)) = Pair (a1 a3,a2 a4)
-- But we can't make an instance of Applicatvie for Pair.
-- Try it and understand why.

-------------------------------------

-- Node Trees: trees with data on the nodes

data NTree a = NLeaf | NNode a (NTree a) (NTree a)
  deriving (Eq,Ord,Show,Read)

instance Functor NTree where
  fmap f NLeaf = NLeaf
  fmap f (NNode x left right) = NNode (f x) (fmap f left) (fmap f right)

-- What about Applicative?
-- Is it possible to define and instance of Applicative for NTree?

-------------------------------------

-- Leaf Trees: trees with data on the leaves

data LTree a = LLeaf a | LNode (LTree a) (LTree a)
  deriving (Eq,Ord,Show,Read)

instance Functor LTree where
  fmap f (LLeaf x) = LLeaf (f x)
  fmap f (LNode left right) = LNode (fmap f left) (fmap f right)

instance Applicative LTree where
  pure = LLeaf
  LLeaf f <*> xs = fmap f xs
  LNode left right <*> xs = LNode (left <*> xs) (right <*> xs)

instance Monad LTree where
  -- >>= :: LTree a -> (a -> LTree b) -> LTree b
  (LLeaf a) >>= f = f a
  (LNode left right) >>= f = LNode (left >>= f) (right >>= f)

a = LNode (LLeaf 1) (LLeaf 2)
c = LNode (LLeaf (+1)) (LLeaf (+2))
d = LNode a (fmap (+1) a)
-------------------------------------

-- Finitely branching trees (with data on the nodes and leaves)

data FTree a = FNode a [FTree a]
  deriving (Eq,Ord,Show,Read)

instance Functor FTree where
  fmap f (FNode x ts) = FNode (f x) (map (fmap f) ts)


b = FNode 1 [FNode 2 [],FNode 3 [],FNode 4 [],FNode 5 []]

-- wrong
instance Applicative FTree where

  pure a = FNode a []
  
  (FNode a []) <*> xs = fmap a xs
  (FNode a as) <*> xs = FNode (a m) (map ((\x -> (\y -> y <*> x)) xs) as) where (FNode m n) = xs

data FFTree a = FFNode (FFTree a) [FFTree a] | FFSingle a 
  deriving (Eq,Ord,Show,Read)

instance Functor FFTree where
  fmap f (FFSingle a) = FFSingle (f a)
  fmap f (FFNode x ts) = FFNode (fmap f x) (map (fmap f) ts)


f = FFNode (FFSingle 1) [FFSingle 2,FFSingle 3,FFSingle 4,FFSingle 5]

-- FFTree could
instance Applicative FFTree where

  pure a = FFNode (FFSingle a) []
  
  (FFSingle a) <*> xs = fmap a xs
  (FFNode a as) <*> xs = FFNode (a <*> xs) (map ((\x -> (\y -> y <*> x)) xs) as)

  
-- Applicative?

-------------------------------------

-- a-branching trees
-- NOT a functor

data ATree a = ALeaf | ANode (a -> ATree a)

-- It's impossible to define an instance of Functor for ATree
-- Think about why this is not a functor

-------------------------------------

-- FIFO lists (first in first out)

data Queue a = Queue [a] [a]
  deriving (Eq,Ord,Show,Read)

-- Think of a pair of lists (Queue [x0,..,xn] [y0,..,ym])
-- as a single queue x0,..,xn,ym,..y0
-- Elements are pushed in front and popped from the end

emptyQ :: Queue a
emptyQ = Queue [] []

push :: a -> Queue a -> Queue a
push x (Queue xs ys) = Queue (x:xs) ys

pop :: Queue a -> (Maybe a, Queue a)
pop (Queue [] []) = (Nothing, emptyQ)
pop (Queue xs (y:ys)) = (Just y, Queue xs ys)
pop (Queue xs []) = pop (Queue [] (reverse xs))

instance Functor Queue where
  fmap g (Queue xs ys) = Queue (fmap g xs) (fmap g ys)

-- Exercise: make an instance of Applicative for Queue

g = Queue [1,2,3,4,5,6,7] [1,2,3,4,5,6,7]

instance Applicative Queue where
  pure x = push x emptyQ
  
  (Queue [] []) <*> _ = emptyQ
  q1 <*> q2 = conn  (b <*> q2)(fmap c q2)
								where 
								(a,b) = pop q1		
								Just c = a

conn :: Queue a -> Queue a -> Queue a
conn (Queue [] []) q2 = q2
conn q1 q2 = conn b (push c q2)
								where 
								(a,b) = pop q1
								Just c = a							
 										
-------------------------------------

-- Wrong fmap for lists

newtype MList a = MList [a]
  deriving (Eq,Ord,Show,Read)

app :: MList a -> MList a -> MList a
app (MList xs) (MList ys) = MList (xs ++ ys)

instance Functor MList where
  fmap g (MList []) = MList []
  fmap g (MList (x:xs)) = fmap g (MList xs) `app` (MList [g x])

-- This definition doesn't satisfy the functor laws

-------------------------------------

-- IO is also a functor, the instance is defined in the Prelude

readInt :: IO Int
readInt = putStrLn "Write a number: " >> readLn

-- Try to run first readInt, then fmap (+13) readInt

-- IO is also an applicative
-- Try: (\x y z -> (x+y)*z) <$> readInt <*> readInt <*> readInt
