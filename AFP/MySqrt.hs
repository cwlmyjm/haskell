module MySqrt where

key :: Fractional a => a
key = 0.0000000001

mysqrt :: (Ord a,Fractional a) => a -> a
mysqrt n = f n 1

f :: (Ord a,Fractional a) => a -> a -> a
f m n = if (abs (m - n)) < key then min m n else f a b
	where 
		a = (m + n)/2
		b = (m * n)/a
		
mysqrt2 :: (Ord a,Fractional a) => a -> a
mysqrt2 n = g n 1 1

g x y z = 	if (x > y * y) then g x (y + z) z
			else
				if (z < key) then y - z
				else g x (y - z) (z / 10)
				
