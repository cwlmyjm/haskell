module L0126 where

myLength :: [a] -> Int
myLength [] = 0
myLength (a:b) = 1 + myLength b

fib :: Num a => [a]
fib = (0:1:[fib!!x+fib!!(x+1)|x<-[0..]])

myFib :: Int -> Int
myFib = \x -> (fib!!(x-1))

myFac 0 = 1
myFac a = a * myFac ((a / abs a)*((abs a) - 1))

type QWQ = [Int]

a ::QWQ
a = [1,2,3]

data Nat = Zero | S Nat

add Zero = id
add (S m) = S.(add m)