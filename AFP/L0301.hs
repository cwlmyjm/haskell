module L0301 where

type State = Int

a,b,c :: State
a = 3
b = 2
c = 1

--type ST = State -> State

--type ST a = State -> (a,State)

newtype ST a = S(State -> (a,State))

app :: ST a -> State -> (a,State)
app (S st) x = st x

instance Functor ST where
  fmap g st = S(\s -> let (x,s') = app st s in (g x,s'))

instance Applicative ST where
  pure x = S (\s-> (x,s))
  
  stf <*> stx = S(\s ->
     let (f,s') = app stf s
         (x,s'') = app stx s' in (f x,s''))

instance Monad ST where
  -- (>>=) :: ST a -> (a -> ST b) -> ST b
  st >>= f = S(\s -> let (x,s') = app st s in app (f x) s')

newtype ST0 s a = ST0 (s -> (a,s))

app0 :: ST0 s a -> s -> (a,s)
app0 (ST0 f) = f

runST0 :: ST0 s a -> s -> a
runST0 st s = fst $ app0 st s

updateST0 :: (s -> s) -> ST0 s ()
updateST0 f = ST0 (\s -> ((),f s))

getST0 :: (s -> a) -> ST0 s a
getST0 g = ST0(\s -> (g s,s))

instance Functor (ST0 s) where
  --fmap :: (a -> b) -> (ST0 s a -> ST0 s b)
  fmap f st = ST0(\s -> let (a,s') = app0 st s in (f a ,s'))
  
instance Applicative (ST0 s) where
  -- pure :: a -> ST0 s a
  pure x = ST0 (\s -> (x,s))
  
  -- <*> :: ST0 s (a -> b) -> ST0 s a -> ST0 s b
  stf <*> stx = ST0 (\s -> 
    let (f,s') = app0 stf s
        (x,s'') = app0 stx s' in (f x,s''))

instance Monad (ST0 s) where
  -- (>>=) :: ST0 s a -> (a -> ST0 s b) -> ST0 s b
  st >>= f = ST0(\s -> let (x,s') = app0 st s in app0 (f x) s')
