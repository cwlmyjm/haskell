module Shudu where

import Data.List

data Cell = Null | Num Int deriving (Show,Eq)

type Block = [[Cell]]

type Grid = [[Block]]

rowOfBlock_Cells :: Int -> Block -> [Cell]
rowOfBlock_Cells a b = b!!a

rowOfGrid_Blocks :: Int -> Grid -> [Block]
rowOfGrid_Blocks a b = b!!a

rowOfGrid_Cells :: Int -> Grid -> [Cell]
rowOfGrid_Cells a b = concat $ map (rowOfBlock_Cells y) (rowOfGrid_Blocks x b)
	where
		x = div a 3
		y = mod a 3
		
a :: Block
a = [[Num 1,Num 4,Num 7],[Num 2,Num 5,Num 8],[Num 3,Num 6,Num 0]]

b :: Grid
b = [[a,a,a],[a,a,a],[a,a,a]]

printGrid :: Grid -> IO()
printGrid a = printGridHelper a 8

printGridHelper :: Grid -> Int -> IO()
printGridHelper a 0 = print $ rowOfGrid_Cells 0 a
printGridHelper a b = do
				print $ rowOfGrid_Cells b a
				printGridHelper a (b-1)
				
isValidBlock :: Block -> Bool
isValidBlock b = and [elem (Num a) (concat b)|a <- [0..8]]

isAllRowInGridValid :: Grid -> Bool
isAllRowInGridValid g = and [and [elem (Num a) (rowOfGrid_Cells r g)|a <- [0..8]]|r <- [0..8]]

isValidGrid :: Grid -> Bool
isValidGrid g = and (map isValidBlock (concat g)) 
				&& isAllRowInGridValid g
				&& isAllRowInGridValid (transpose (map (map transpose) g))

myTranspoe :: [[a]] -> [[a]]
myTranspoe [] = []
myTranspoe [a] = map (\x -> [x]) a
myTranspoe (a:b) = map (\(x,y) -> x:y) $ zip a (myTranspoe b)

isFull :: Grid -> Bool
isFull g = not $ elem Null (concat [rowOfGrid_Cells r g|r <- [0..8]])

