module L0222 where

import L0215
import L0216

prods0 xs ys = [x*y|x <- xs, y <- ys]

prods1 xs ys = pure (*) <*> xs <*> ys

--instance Applicative Tree1 where
-- pure = Leaf1
-- <*>