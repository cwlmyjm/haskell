module Main where


import Prelude hiding (Maybe,lookup)
import Data.Char (isSpace)
import Data.List (inits,intersperse)

import Parser
import Utilities




data Prop = VAR String | F | T | NOT Prop | AND Prop Prop 
            deriving (Show, Read, Eq)

-- Examples:
p1, p2 :: Prop
p1 = AND (VAR "p") T
p2 = NOT (AND (VAR "q") (NOT (VAR "q")))

-- Simple parser (assuming the argument correctly represents a proposition in Choof syntax)
-- Use the function parse and the parser pProp to give a single-line definition. 
readProp :: String -> Prop
-- Example: 
--      readProp "~(q && ~q)" = NOT (AND (VAR "q") (NOT (VAR "q")))
readProp = fst . head . parse pProp

-- Examples
q1,q2 :: Prop
q1 = readProp "p && T"
q2 = readProp "~(q && ~q)"  -- This is a tautology!

-- Pretty printer
showProp :: Prop -> String
-- Example: 
-- showProp (NOT (AND (VAR "q") (NOT (VAR "q")))) = "~(q && ~q)"
showProp (VAR v)          = v 
showProp T                = "T"
showProp F                = "F"
showProp (NOT p)          = '~' : showProp p
showProp (AND p q)        = concat ["(",showProp p," && ",showProp q,")"]



main :: IO ()
main = 
    do
    s <- readFile "proof.txt"
    putStrLn $ process s
    return ()


readProp' :: String -> Prop
-- Same as readProp, except that more information is given in case of a parse error.
--
readProp' s = case parse pProp s of 
                [(a,"")] -> a
                r        -> error $ unlines $ ["Parsing : " ++ s,"No parse : " ++ show r]

process :: String -> String
process text = unlines $ 
               [ concat $ intersperse " | "  
                    [ pad 40 $ showProp   conclusion 
                    , pad 6  $ show     $ isTautology conclusion 
                    , pad 6  $ show     $ isDerivable premisses conclusion] 
               | (premisses,conclusion) <- map initlast $ tail $ inits propositions ]
    where
    propositions = map readProp' $ filter (not . all isSpace) $ lines text
    initlast xs  = (init xs, last xs) -- initlast [p1,p2,p3,p4] = ([p1,p2,p3],p4)
    pad n s = s ++ take (n-length s) (repeat ' ')

eval :: [(String,Bool)] -> Prop -> Bool
eval env (VAR v)          = lookup' v env -- See module Utilities
eval env (NOT p)          = not (eval env p)
eval env (AND p q)        = (eval env p) &&  (eval env q)
eval env T                = True
eval env F                = False



variables :: Prop -> [String]
-- (similar to valuesOf for binary trees defined in Lab 7)
variables (VAR v)          = [v] 
variables T                = []
variables F                = []
variables (NOT p)          = variables p
variables (AND p q)        = variables p ++> variables q

(++>) :: Ord a => [a] -> [a] -> [a]
-- Merges two sorted lists without repetitions into one (same as merge' Lab 7)
(x:xs) ++> (y:ys) | x == y = x : (xs     ++> ys)
                  | x <  y = x : (xs     ++> (y:ys))
                  | x >  y = y : ((x:xs) ++> ys)
xs     ++> ys              = xs ++ ys





isTautology :: Prop -> Bool
isTautology p = and [ eval e p | e <- environments ]
    where
    environments = map (zip vars)  (bss (length vars))
    vars = variables p

bss :: Int -> [[Bool]]
-- bss n = the list of lists of boolean values of length n 
bss 0 = [[]]
bss n = [b:bs | bs <- bss (n-1), b<-[True,False]]


isDerivable :: [Prop] -> Prop -> Bool
-- justifiable ps c = Proposition c is derivable from the propositions in the list ps.
-- Reminder: a proposition c is derivalbe from propositions p1,p2,...,pn if and only if
--           c is true whenever p1,p2,...,pn are true.
--           More precisely, c is derivable from p1,p2,...,pn 
--           if and only if the following holds:
--
--           For every environment  env 
--           of all variables appearing  in c,p1,p2,...,pn, 
--           if p1,p2,...,pn are true in env then c is also true in env.

isDerivable premisses conclusion = and 
                                    [ eval env conclusion 
                                    | env <- map (zip vars) (bss (length vars))
                                    , and $ map (eval env) premisses  ]
                                where
                                    vars = foldr1 (++>) $ map variables $ conclusion:premisses

truthtable :: Prop -> String
truthtable p = unlines $ map (concat . intersperse " | ") $
               (vars ++ [showp])
               :
               [ map (showBool . snd) e ++ [pad (length showp) $ showBool $ eval e p]
               | e <- environments]
             where 
             environments = map (zip vars)  (bss (length vars))
             vars = variables p
             showBool True  = "T"
             showBool False = "F"
             pad n s = s ++ take (n-length s) (repeat ' ')
             showp = showProp p


test :: IO()
test = putStrLn $ truthtable $ readProp "(~q && (p && T)) && ~(r && ~q)"

{- running test should output something like:

p | q | r | ((~q && (p && T)) && ~(r && ~q))
T | T | T | F                               
F | T | T | F                               
T | F | T | F                               
F | F | T | F                               
T | T | F | F                               
F | T | F | F                               
T | F | F | T                               
F | F | F | F  
-}
-- Parser
pProp :: Parser Prop
pProp = do 
        t <- pTerm
        do symbol "&&"
           u <- pProp
           return (AND t u)
         +++ return t

 
pTerm = pNegation +++ pAtom +++ brackets pProp

pAtom = pConstant +++ pVariable

pNegation = 
    do
    symbol "~"
    v <- pTerm
    return (NOT  v)

pConstant = pTrue +++ pFalse

pTrue =
    do
    symbol "T"
    return T

pFalse =
    do
    symbol "F"
    return F

pVariable =
    do
    v <- identifier
    return $ VAR v








