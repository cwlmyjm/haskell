module Turing where

import Prelude hiding (Left, Right, until)

import Data.List

type Symbol        = Char
type Tape          = ([Symbol], Symbol, [Symbol])
type Input         = [Symbol]

data State         = Initial | Final | S String  deriving (Eq, Show)
data Direction     = Left | Right | Stay deriving Show
type Machine       = (State, Symbol) -> (Symbol, Direction, State)

type Configuration = (State, Tape)



-- * Tape operations
readTape :: Tape -> Symbol
readTape (_,c,_) = c

writeTape :: Symbol -> Tape -> Tape
writeTape c (xs,_,ys) = (xs,c,ys)

moveCursor :: Direction -> Tape -> Tape
moveCursor Left  (x:xs, c,   ys) = (  xs, x , c:ys)  
moveCursor Right (  xs, c, y:ys) = (c:xs, y ,   ys)
moveCursor _     (  xs, c,   ys) = (  xs, c,    ys) 

initialTape :: Input -> Tape
initialTape s = ([],c,s') where (c : s') = s ++ repeat ' '

-- * Running a machine
run :: Machine -> Input -> Configuration
run m = head . filter isFinished . iterate (step m) . initialConfiguration

-- or simply 
-- run m = until isFinished (step m) .  initialConfiguration
-- if you know about until

-- until is predefined but you could define it as 
until :: (a -> Bool) -> (a -> a) -> a -> a
until p f = head . filter p . iterate f


trace :: Machine -> Input -> [Configuration]
trace m s = rs ++ [r]
               where 
               (rs,r:_) = (break isFinished . iterate (step m) . initialConfiguration) s
    
initialConfiguration :: Input -> Configuration
initialConfiguration s = (Initial, initialTape s)
                         
step ::  Machine -> Configuration -> Configuration
step m (s,t) = (s' , moveCursor d (writeTape c' t)) 
               where 
			   (c',d,s') = m (s,readTape t)

isFinished :: Configuration -> Bool
isFinished = (==Final) . fst



-- * IO operations 

-- (IO = Input/Output)
main :: FilePath -> Input -> IO()
main f input = 
 do
 s <- readFile f 
 putStrLn $ unlines $ map showConfiguration (trace (readMachine s) input)
 return ()

readFromFile :: FilePath -> IO()
readFromFile f = 
 do
 s <- readFile f 
 putStrLn $ unlines$ [ "f (" ++ show a ++ "\t, " ++ show b ++ ")\t=\t(" ++ show c ++ "\t, " ++ show d ++ "\t, " ++ show e ++ ")"    | ((a,b),(c,d,e)) <- readAssocs s]
 return ()

showConfiguration :: Configuration -> String
showConfiguration (s,(xs,c,ys)) = pad 10 (showState s) ++ vs ++ phi xs1 ++ zs ++ phi xs2 ++ "[" ++ [c] ++ "]" ++ phi (take n ys) ++ "   " ++ show lx
        where phi = intersperse '.'
              lx  = length xs
              w   = 20
              p   = 10
              xs1 = take m1 (reverse xs)
              xs2 = reverse (take m2 xs)
              vs = if null xs then "" else "." 
              (m1,zs,m2,n) = if lx > 50 then (p,"///",w-p-1,w) else (0,"",w,2*w-lx) 
              pad i s = take i (s ++ repeat ' ')
              showState Final    = "Final"
              showState Initial  = "Initial"
              showState (S s)    = s
              
-- * Building machines 
readMachine :: String -> Machine
readMachine = fromAssocs . readAssocs

readAssocs  = (map readTransition) . filter (not . isBlank) . lines
                where
                isBlank = null . fst . break (=="--") . words  
                readTransition xs = ((phi s,rho c),(rho c',psi d,phi s')) 
                                        where
                                        (s:c:c':d:s':_) = words xs
                                        phi "#" = Initial
                                        phi "." = Final
                                        phi s   = S s
                                        psi ">" = Right
                                        psi "<" = Left
                                        psi "-" = Stay
                                        rho "~" = ' '
                                        rho [c] = c

-- compose :: Machine -> Machine -> Machine
-- compose m2 m1 = map (rename (f 1)) m1 ++ map (rename (f 2)) m2
-- where
-- rename f ((s,c),(c',d,s'))  = ((f s,c),(c',d,f s'))
-- f 1 Final = S "Start2"
-- f 2 Initial  = f 1 Final
-- f i (S s)  = S (s ++ show i)
-- f _ s      = s

-- * General functions 
-- (By "general", we mean functions whose type is independent of specific types defined in this module.)

find' :: (a -> Bool) -> [a] -> a
-- | find p xs returns the first element in xs which satisfies the property p. The list xs should be infinite or possess an element satisfying p.
find' p = head . filter p
              
fromAssocs :: Eq a => [(a,b)] -> a -> b
-- | fromAssocs xs returns the partial function whose graph is the set of pairs in the list xs.
--   First components of pairs should not appear twice in the list xs. That is, the list must indeed represent a partial function 
--   from a to b. In Haskell dialect, we should have :    nub (map fst xs) = map fst xs 
fromAssocs assocs sc = snd  (find' ((==sc).fst) assocs)

    